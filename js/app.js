var config = {
    apiKey: "AIzaSyAZR8oWRJCgXngz0ekjnF-HJZIsxP_d1Ws",
    authDomain: "karydakis-71a8c.firebaseapp.com",
    databaseURL: "https://karydakis-71a8c.firebaseio.com",
    projectId: "karydakis-71a8c",
    storageBucket: "karydakis-71a8c.appspot.com",
    messagingSenderId: "954779226099"
};
firebase.initializeApp(config);

var app = angular.module('karydakis', ["ngRoute","firebase"]);
var db = firebase.database();

app.factory("Auth", ["$firebaseAuth",
    function($firebaseAuth) {
        return $firebaseAuth();
    }
]);

app.config(function($routeProvider) {
    $routeProvider
        .when("/", {
            templateUrl : "views/user.html",
            controller: "UsersController",
            resolve: {
                "currentAuth": ["Auth", function(Auth) {
                    return Auth.$requireSignIn();
                }]
            }
        })
        .when("/login", {
            templateUrl : "views/login.html",
            controller: "LoginController"
        })
        .when("/tasks", {
            templateUrl : "views/tasks.html",
            controller: "TasksController",
            resolve: {
                "currentAuth": ["Auth", function(Auth) {
                    return Auth.$requireSignIn();
                }]
            }
        })
        .when("/users", {
            templateUrl : "views/users.html",
            controller: "UsersController",
            resolve: {
                "currentAuth": ["Auth", function(Auth) {
                    return Auth.$requireSignIn();
                }]
            }
        })
        .when("/drive", {
            templateUrl : "views/drive.html",
            controller: "DriveController",
            resolve: {
                "currentAuth": ["Auth", function(Auth) {
                    return Auth.$requireSignIn();
                }]
            }
        })
        .otherwise({
            template : "<h1>404</h1>"
        });
});


app.run(function($rootScope, $location){
    $rootScope.$on("$routeChangeError", function(event, next, previous, error) {
        if (error === "AUTH_REQUIRED") {
            $location.path("/login");
        }
    });

    $rootScope.messaging = firebase.messaging();
    $rootScope.messaging.requestPermission()
    .then(function(){
        return $rootScope.messaging.getToken();
    })
    .then(function(token){
        $rootScope.token = token;
    })
    .catch(function(){
        console.log('TON HPIAME');
    });
});