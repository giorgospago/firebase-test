app.controller("MainController", function ($scope, $timeout, $rootScope, $firebaseAuth, $firebaseObject, $location) {
    
    $scope.auth = $firebaseAuth();

    $rootScope.user = {};

    $scope.logout = function () {
        $scope.auth.$signOut();
        $location.path("/login");
    };

    $scope.auth.$onAuthStateChanged(function (user) {
        if (user) {
            var ref = db.ref('users/' + user.uid);
            $scope.authUserObj = $firebaseObject(ref);
            $scope.authUserObj.$bindTo($rootScope, 'user');

            // clear forms
            $scope.register = { gender: "male" };
            $scope.form = {};

            $location.path("/");
        }
        else {
            if ($scope.authUserObj) $scope.authUserObj.$destroy();
            $rootScope.user = null;
        }
    });




    $rootScope.not = null;
    
    $rootScope.messaging
    .onMessage(function(payload) {
        console.log(payload);
        $rootScope.not = payload.notification;
        $rootScope.$apply();


        $timeout(function(){
            $rootScope.not = null;
        }, 3000);
    });
});