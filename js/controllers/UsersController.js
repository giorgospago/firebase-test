app.controller("UsersController", function ($scope, $rootScope, $firebaseObject) {
    
    $scope.users = [];
    
    $scope.getUsers = function(){
        $firebaseObject(db.ref('users')).$bindTo($scope, "users");
    };

});