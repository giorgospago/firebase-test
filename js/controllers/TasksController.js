app.controller("TasksController", function ($scope, $rootScope, $firebaseObject, $firebaseStorage) {
    $scope.tasks = [];
    $firebaseObject(db.ref('tasks/'+$scope.user.$id)).$bindTo($scope, "tasks");

    $scope.task = {
        title: ""
    };

    var storageRef = firebase.storage().ref("tasks/"+$rootScope.user.$id+"/");

    $scope.taskErr = null;
    $scope.saveTask = function(){
        $scope.taskErr = null;
        $scope.uploadProgress = 0;

        // upload file to storage
        var myFile = document.getElementById('myFile');
        var file = myFile.files[0];

        $scope.fileRef = $firebaseStorage(storageRef.child(file.name));
        var uploadTask = $scope.fileRef.$put(file);
        
        uploadTask.$progress(function(snapshot) {
            $scope.uploadProgress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        });

        uploadTask.$complete(function(data){

            db.ref('tasks/'+$scope.user.$id)
            .push({
                title: $scope.task.title,
                photo: data.downloadURL
            })
            .catch(function(err){
                $scope.taskErr = err;
            });

            $scope.task = {
                title: ""
            };

        });
    };


    $scope.removeTask = function(taskId){
        db.ref('tasks/'+$scope.user.$id+"/"+taskId).remove();
    };
});