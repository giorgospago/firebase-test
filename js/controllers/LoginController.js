app.controller("LoginController", function ($scope, $rootScope, $firebaseAuth) {
    $scope.auth = $firebaseAuth();

    $scope.socialErr = null;
    $scope.socialLogin = function (provider) {
        $scope.socialErr = null;
        $scope.auth.$signInWithPopup(provider)
            .then(function (firebaseUser) {
                var uid = firebaseUser.user.uid;
                var newUser = {
                    name: firebaseUser.additionalUserInfo.profile.name,
                    email: firebaseUser.additionalUserInfo.profile.email,
                    picture: firebaseUser.additionalUserInfo.profile.picture,
                    gender: firebaseUser.additionalUserInfo.profile.gender,
                    provider: firebaseUser.additionalUserInfo.providerId
                };
                db.ref('users/' + uid).update(newUser);
            })
            .catch(function(err){
                $scope.socialErr = err.message;
            });
    };

    // Register
    $scope.register = { gender: "male" };
    $scope.submitRegister = function () {
        $scope.auth.$createUserWithEmailAndPassword($scope.register.email, $scope.register.password)
            .then(function (firebaseUser) {
                var uid = firebaseUser.uid;
                var newUser = {
                    name: $scope.register.name,
                    email: firebaseUser.email,
                    picture: $scope.register.picture,
                    gender: $scope.register.gender,
                    provider: "email.password"
                };
                db.ref('users/' + uid).set(newUser);
            })
            .catch(function(err){
                $scope.regErr = err.message;
            });
    };

    // Login
    $scope.form = {};
    $scope.loginErr = null;
    $scope.emailLogin = function () {
        $scope.loginErr = null;
        $scope.auth
            .$signInWithEmailAndPassword($scope.form.email, $scope.form.password)
            .catch(function(err){
                $scope.loginErr = err.message;
            });
    };
});